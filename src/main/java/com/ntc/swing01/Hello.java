/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ntc.swing01;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 *
 * @author L4ZY
 */
public class Hello {

    public static void main(String[] args) {
        JFrame frmMain = new JFrame("Hello");
        frmMain.setSize(600, 500);
        frmMain.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JLabel lblName = new JLabel("Name :");
        lblName.setSize(60, 25);
        lblName.setLocation(5, 5);
        JLabel lblAge = new JLabel("Age :");
        lblAge.setSize(60, 25);
        lblAge.setLocation(5, 50);
        JLabel lblOut = new JLabel("Hi:");
        lblOut.setSize(250, 250);
        lblOut.setLocation(75, 200);

        JTextField txtName = new JTextField();
        txtName.setSize(200, 25);
        txtName.setLocation(70, 5);

        JTextField txtAge = new JTextField();
        txtAge.setSize(50, 25);
        txtAge.setLocation(70, 50);

        JButton btnReset = new JButton("RESET");
        btnReset.setSize(100, 60);
        btnReset.setLocation(10, 100);
        btnReset.setBackground(Color.gray);

        JButton btnConfirm = new JButton("CONFIRM");
        btnConfirm.setSize(100, 60);
        btnConfirm.setLocation(160, 100);
        btnConfirm.setBackground(Color.green);

        JButton btnExit = new JButton("EXIT");
        btnExit.setSize(100, 60);
        btnExit.setLocation(310, 100);
        btnExit.setBackground(Color.red);

        frmMain.setLayout(null);
        frmMain.add(lblName);
        frmMain.add(txtName);
        frmMain.add(txtAge);
        frmMain.add(lblAge);
        frmMain.add(lblOut);
        frmMain.add(btnReset);
        frmMain.add(btnConfirm);
        frmMain.add(btnExit);

        btnConfirm.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String name = txtName.getText();
                String age = txtAge.getText();
                lblOut.setText("Hello " + name + " " + " and Your are " + age + " years old ");
                lblOut.show();
            }
        });
        btnReset.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                txtName.setText(" ");
                txtAge.setText(" ");
            }
        });

        btnExit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               System.exit(0); 
            }
        });

        frmMain.setVisible(true);

    }

}
